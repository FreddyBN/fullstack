-- 1. Escriba una Query que entregue la lista de  alumnos para el curso "programación".
	SELECT 
    		a.ID_ALUMNO, a.NOMBRE 
        FROM 
        	ALUMNOS AS a 
            INNER JOIN CURSOS_ALUMNOS AS ca ON (ca.ID_ALUMNO = a.ID_ALUMNO) 
            INNER JOIN CURSOS AS c ON (c.ID_CURSO = ca.ID_CURSO) 
        WHERE 
        	c.NOMBRE = "programación";

-- 2. Escriba una Query que calcule el promedio de notas de un alumno en un curso.
	SELECT 
		a.NOMBRE AS ALUMNO, c.NOMBRE AS CURSO, AVG(p.NOTA) AS PROMEDIO
	FROM 
        	PRUEBAS_ALUMNOS AS p 
            	INNER JOIN CURSOS_ALUMNOS AS ca ON (ca.ID = p.ID_CURSO_ALUMNO)
		INNER JOIN CURSOS AS c ON (c.ID_CURSO = ca.ID_CURSO)
		INNER JOIN ALUMNOS AS a ON (a.ID_ALUMNO = ca.ID_ALUMNO)
        WHERE 
        	ca.ID_CURSO = 1 -- ID DEL CURSO 
	        AND ca.ID_ALUMNO = 1; -- ID DEL ALUMNO

-- 3. Escriba una Query que entregue a los alumnos y el promedio que tiene en cada curso.
	SELECT 
		a.NOMBRE AS ALUMNO, c.NOMBRE AS CURSO, AVG(p.NOTA) AS PROMEDIO
	FROM 
        	PRUEBAS_ALUMNOS AS p 
	        INNER JOIN CURSOS_ALUMNOS AS ca ON (ca.ID = p.ID_CURSO_ALUMNO)
	        INNER JOIN CURSOS AS c ON (c.ID_CURSO = ca.ID_CURSO)
		INNER JOIN ALUMNOS AS a ON (a.ID_ALUMNO = ca.ID_ALUMNO)
        WHERE 
		ca.ID_ALUMNO = 1 -- ID DEL ALUMNO
        GROUP BY 
		ca.ID_ALUMNO, ca.ID_CURSO;

-- 4. Escriba una Query que lista a todos los alumnos con más de un curso con promedio rojo.
	SELECT 
		a.NOMBRE AS ALUMNO, c.NOMBRE AS CURSO, AVG(p.NOTA) AS PROMEDIO
	FROM 
	    	PRUEBAS_ALUMNOS AS p
		INNER JOIN CURSOS_ALUMNOS AS ca ON (ca.ID = p.ID_CURSO_ALUMNO)
		INNER JOIN CURSOS AS c ON (c.ID_CURSO = ca.ID_CURSO)
		INNER JOIN ALUMNOS AS a ON (a.ID_ALUMNO = ca.ID_ALUMNO)
	GROUP BY 
    		ca.ID_ALUMNO, ca.ID_CURSO
	HAVING 
    		AVG(p.NOTA) <= 10 -- NOTA MÁXIMA DE CONSIDERACIÓN PROMEDIO ROJO

-- 5. Dejando de lado el problema del cólegio se tiene una tabla con información de jugadores de tenis:
-- `PLAYERS(Nombre, Pais, Ranking)`. Suponga que Ranking es un número de 1 a
-- 100 que es distinto para cada jugador. Si la tabla en un momento dado tiene
-- solo 20 registros, indique cuantos registros tiene la tabla que resulta de la siguiente consulta:

```
SELECT c1.Nombre, c2.Nombre
FROM PLAYERS c1, PLAYERS c2
WHERE c1.Ranking > c2.Ranking
```
-- Seleccione las respuestas correctas:

```
a) 400
b) 190
c) 20
d) imposible saberlo
```

-- OPCIÓN B: 190. En otras palabras, es la sumatoria de números desde el 19 hasta el 1 (Por encima de cuantos juegadores según el ranking).
-- El #1, está sobre 19. El #2 sobre 18...... El #13 sobre 7 y así para el resto.

-- -----------------------------------------------------------------------------------------------------------------------------------  --
-- Un colegio necesita un sistema para administrar sus cursos. El sistema tiene que soportar que se ingresen varios cursos. 		--
-- Cada curso tendrá un profesor a cargo y una serie de alumnos inscritos. 								--
-- Cada profesor, así como cada alumno puede estar en más de un curso. Además cada curso tendrá una cantidad no determinada de pruebas,	--
-- y el sistema debe permitir ingresar la nota para cada alumno en cada prueba. Todas las pruebas valen lo mismo.			--
-- -----------------------------------------------------------------------------------------------------------------------------------  --


-- APLICADO Y CREADO EN BASE DE DATOS MYSQL --

--
-- Base de datos: `colegio`
--
CREATE DATABASE IF NOT EXISTS `colegio` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `colegio`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ALUMNOS`
--

CREATE TABLE `ALUMNOS` (
  `ID_ALUMNO` int(11) NOT NULL,
  `NOMBRE` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CURSOS`
--

CREATE TABLE `CURSOS` (
  `ID_CURSO` int(11) NOT NULL,
  `NOMBRE` varchar(150) CHARACTER SET latin1 NOT NULL,
  `ID_PROFESOR` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CURSOS_ALUMNOS`
--

CREATE TABLE `CURSOS_ALUMNOS` (
  `ID` int(11) NOT NULL,
  `ID_CURSO` int(11) NOT NULL,
  `ID_ALUMNO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PROFESORES`
--

CREATE TABLE `PROFESORES` (
  `ID_PROFESOR` int(11) NOT NULL,
  `NOMBRE` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PRUEBAS`
--

CREATE TABLE `PRUEBAS` (
  `ID_PRUEBA` int(11) NOT NULL,
  `NOMBRE` varchar(100) COLLATE utf8_general_ci NOT NULL,
  `ID_CURSO` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PRUEBAS_ALUMNOS`
--

CREATE TABLE `PRUEBAS_ALUMNOS` (
  `ID` int(11) NOT NULL,
  `ID_PRUEBA` int(11) NOT NULL,
  `ID_CURSO_ALUMNO` int(11) NOT NULL,
  `NOTA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ALUMNOS`
--
ALTER TABLE `ALUMNOS`
  ADD PRIMARY KEY (`ID_ALUMNO`);

--
-- Indices de la tabla `CURSOS`
--
ALTER TABLE `CURSOS`
  ADD PRIMARY KEY (`ID_CURSO`),
  ADD KEY `ID_PROFESORES_IDX` (`ID_PROFESOR`);

--
-- Indices de la tabla `CURSOS_ALUMNOS`
--
ALTER TABLE `CURSOS_ALUMNOS`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_CURSO_IDX` (`ID_CURSO`),
  ADD KEY `ID_ALUMNO_IDX` (`ID_ALUMNO`);

--
-- Indices de la tabla `PROFESORES`
--
ALTER TABLE `PROFESORES`
  ADD PRIMARY KEY (`ID_PROFESOR`);

--
-- Indices de la tabla `PRUEBAS`
--
ALTER TABLE `PRUEBAS`
  ADD PRIMARY KEY (`ID_PRUEBA`),
  ADD KEY `ID_CURSO_PRUEBA_IDX` (`ID_CURSO`);

--
-- Indices de la tabla `PRUEBAS_ALUMNOS`
--
ALTER TABLE `PRUEBAS_ALUMNOS`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_PRUEBA_IDX` (`ID_PRUEBA`),
  ADD KEY `ID_CURSO_ALUMNO_IDX` (`ID_CURSO_ALUMNO`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ALUMNOS`
--
ALTER TABLE `ALUMNOS`
  MODIFY `ID_ALUMNO` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `CURSOS`
--
ALTER TABLE `CURSOS`
  MODIFY `ID_CURSO` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `CURSOS_ALUMNOS`
--
ALTER TABLE `CURSOS_ALUMNOS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `PROFESORES`
--
ALTER TABLE `PROFESORES`
  MODIFY `ID_PROFESOR` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `PRUEBAS`
--
ALTER TABLE `PRUEBAS`
  MODIFY `ID_PRUEBA` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `PRUEBAS_ALUMNOS`
--
ALTER TABLE `PRUEBAS_ALUMNOS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `CURSOS`
--
ALTER TABLE `CURSOS`
  ADD CONSTRAINT `fk_id_profesores` FOREIGN KEY (`ID_PROFESOR`) REFERENCES `PROFESORES` (`ID_PROFESOR`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `CURSOS_ALUMNOS`
--
ALTER TABLE `CURSOS_ALUMNOS`
  ADD CONSTRAINT `fk_id_alumno` FOREIGN KEY (`ID_ALUMNO`) REFERENCES `ALUMNOS` (`ID_ALUMNO`),
  ADD CONSTRAINT `fk_id_curso` FOREIGN KEY (`ID_CURSO`) REFERENCES `CURSOS` (`ID_CURSO`);

--
-- Filtros para la tabla `PRUEBAS`
--
ALTER TABLE `PRUEBAS`
  ADD CONSTRAINT `fk_id_curso_prueba` FOREIGN KEY (`ID_CURSO`) REFERENCES `CURSOS` (`ID_CURSO`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `PRUEBAS_ALUMNOS`
--
ALTER TABLE `PRUEBAS_ALUMNOS`
  ADD CONSTRAINT `fk_id_curso_alumno` FOREIGN KEY (`ID_CURSO_ALUMNO`) REFERENCES `CURSOS_ALUMNOS` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_id_prueba` FOREIGN KEY (`ID_PRUEBA`) REFERENCES `PRUEBAS` (`ID_PRUEBA`) ON DELETE NO ACTION ON UPDATE NO ACTION;

$(document).ready(function($){

	var inicio = "08:00";
	var fin = "20:30";
	var intervalo = "30";

	addDivHoras();
	addDivDias();
	llenarAgenda();

	function addDivHoras (){ 
		$.each(generateDates(inicio, fin, intervalo), function (i, d) {
			$( "<div class='hora'>"+hhmm(d)+"</div>" ).appendTo( ".flechas" );
		});
	}

	function addDivDias(){
		$.each(generateDates(inicio, fin, intervalo), function (i, d) {
			$( "<div class='dia' data-hora='"+hhmm(d)+"'></div>" ).appendTo( ".contenedor-dia" );
		});
	}

	function llenarAgenda(){
		$.getJSON("js/agenda.json", function( data ) {
			$.each(data, function(dia, datos) {
				$.each(datos, function(key, value) {
					
					var celda = $('div[data-dia="' + dia + '"] div[data-hora="' + value.start_time + '"]');
					//var celda = EncontrarHorario(dia, value.start_time);

					console.log(celda);

					$(celda).addClass('celda celda-agendado').text(value.name);

					console.log(dia + " - " + value.name + " - " + value.start_time + " - " + value.end_time);
				});
			});
		});
	}

	function generateDates(start, end, interval) {
		var numOfRows = Math.ceil(timeDiff(start, end) / interval);
		return $.map(new Array(numOfRows), function (_, i) {
			return new Date(new Date(2000, 0, 1, start.split(':')[0], start.split(':')[1]).getTime() + i * interval * 60000);
		});
	}

	function timeDiff(start, end) {
    	return (new Date(2000, 0, 1, end.split(':')[0], end.split(':')[1]).getTime() -
            new Date(2000, 0, 1, start.split(':')[0], start.split(':')[1]).getTime()) / 60000;
  	}

	function hhmm(date) {
		var hours = date.getHours()
		, minutes = date.getMinutes();
		return ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2);
	}

	function EncontrarHorario(day, start){
		var celda = $('div[data-dia="' + day + '"] div[data-hora="' + start + '"]')[0];
		
		if (celda != undefined){
			$(celda).html;
			return celda;
		}else{
			return null;
		}
	}
});
from django import forms

from colegio.models import Alumnos, Profesores, Pruebas, Cursos, Pruebas_Alumnos, Cursos_Alumnos, PromedioRojo

class AlumnosForm(forms.ModelForm):
	class Meta:
		model = Alumnos
		fields = [
			'NOMBRE',
		]
		labels = {
			'nombre': 'Nombres',
		}
		widgets = {
			'nombre': forms.TextInput(attrs={'class':'form-control'})
		}

class ProfesoresForm(forms.ModelForm):
	class Meta:
		model = Profesores
		fields = [
			'NOMBRE',
		]
		labels = {
			'nombre': 'Nombres',
		}
		widgets = {
			'nombre': forms.TextInput(attrs={'class':'form-control'})
		}

class PruebasForm(forms.ModelForm):
	class Meta:
		model = Pruebas
		fields = [
			'NOMBRE',
			'ID_CURSO',
		]
		labels = {
			'nombre': 'Nombre de Prueba',
			'ID_CURSO': 'Curso',
		}
		widgets = {
			'nombre': forms.TextInput(attrs={'class':'form-control'}),
			'ID_CURSO': forms.Select(attrs={'class':'form-control'}),
		}

class CursosForm(forms.ModelForm):
	class Meta:
		model = Cursos
		fields = [
			'NOMBRE',
			'ID_PROFESOR',
		]
		labels = {
			'nombre': 'Nombre de Prueba',
			'ID_PROFESOR': 'Profesor',
		}
		widgets = {
			'nombre': forms.TextInput(attrs={'class':'form-control'}),
			'ID_PROFESOR': forms.Select(attrs={'class':'form-control'}),
		}

class NotasForm(forms.ModelForm):
	class Meta:
		model = Pruebas_Alumnos
		fields = [
			'ID_PRUEBA',
			'ID_CURSO_ALUMNO',
			'NOTA',
		]
		labels = {
			'ID_PRUEBA': 'Prueba Aplicada',
			'ID_CURSO_ALUMNO': 'Alumno',
			'NOTA': 'Nota',
		}
		widgets = {
			'ID_PRUEBA': forms.Select(attrs={'class':'form-control'}),
			'ID_CURSO_ALUMNO': forms.Select(attrs={'class':'form-control'}),
			'NOTA': forms.TextInput(attrs={'class':'form-control'}),
		}

class CursosAlumnosForm(forms.ModelForm):
	class Meta:
		model = Cursos_Alumnos
		fields = [
			'ID_ALUMNO',
			'ID_CURSO',
		]
		labels = {
			'ID_ALUMNO': 'Alumno',
			'ID_CURSO': 'Curso',
		}
		widgets = {
			'ID_ALUMNO': forms.Select(attrs={'class':'form-control'}),
			'ID_CURSO': forms.Select(attrs={'class':'form-control'}),
		}

class PromedioRojoForm(forms.ModelForm):
	class Meta:
		model = PromedioRojo
		fields = [
			'PROMEDIO',
		]
		labels = {
			'PROMEDIO': 'Promedio Rojo'
		}
		widgets = {
			'PROMEDIO' : forms.TextInput(attrs={'class':'form-control'})
		}
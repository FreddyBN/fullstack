from django.conf.urls import include
from django.conf.urls import url
from . import views

app_name = 'colegio'

urlpatterns = [
	#Home - Página Inicio
	url(r'^$', views.home, name='home'),
	#Módulo de Alumnos
	url(r'^alumnos$', views.AlumnosList.as_view(), name='alumnos'),
	url(r'^alumnos/nuevo$', views.AlumnosCreate.as_view(), name='nuevo_alumno'),
	url(r'^alumnos/editar/(?P<pk>\d+)/$', views.AlumnosUpdate.as_view(), name='editar_alumno'),
	url(r'^alumnos/eliminar/(?P<pk>\d+)/$', views.AlumnosDelete.as_view(), name='eliminar_alumno'),
	#Módulo de Cursos
	url(r'^cursos/$', views.CursosList.as_view(), name='cursos'),
	url(r'^cursos/nuevo$', views.CursosCreate.as_view(), name='nuevo_curso'),
	url(r'^cursos/editar/(?P<pk>\d+)/$', views.CursosUpdate.as_view(), name='editar_curso'),
	url(r'^cursos/eliminar/(?P<pk>\d+)/$', views.CursosDelete.as_view(), name='eliminar_curso'),
	#Módulo de Profesores
	url(r'^profesores/$', views.ProfesoresList.as_view(), name='profesores'),
	url(r'^profesores/nuevo$', views.ProfesoresCreate.as_view(), name='nuevo_profesor'),
	url(r'^profesores/editar/(?P<pk>\d+)/$', views.ProfesoresUpdate.as_view(), name='editar_profesor'),
	url(r'^profesores/eliminar/(?P<pk>\d+)/$', views.ProfesoresDelete.as_view(), name='eliminar_profesor'),
	#Módulo de las Pruebas
	url(r'^pruebas/$', views.PruebasList.as_view(), name='pruebas'),
	url(r'^pruebas/nuevo$', views.PruebasCreate.as_view(), name='nuevo_prueba'),
	url(r'^pruebas/editar/(?P<pk>\d+)/$', views.PruebasUpdate.as_view(), name='editar_prueba'),
	url(r'^pruebas/eliminar/(?P<pk>\d+)/$', views.PruebasDelete.as_view(), name='eliminar_prueba'),
	#Módulo de Notas
	url(r'^notas/$', views.NotasList.as_view(), name='notas'),
	url(r'^notas/nuevo$', views.NotasCreate.as_view(), name='nuevo_nota'),
	url(r'^notas/editar/(?P<pk>\d+)/$', views.NotasUpdate.as_view(), name='editar_nota'),
	url(r'^notas/eliminar/(?P<pk>\d+)/$', views.NotasDelete.as_view(), name='eliminar_nota'),
	#Módulo de Cursos Alumnos
	url(r'^cursos_alumnos/$', views.CursosAlumnosList.as_view(), name='cursos_alumnos'),
	url(r'^cursos_alumnos/nuevo$', views.CursosAlumnosCreate.as_view(), name='nuevo_cursos_alumnos'),
	url(r'^cursos_alumnos/editar/(?P<pk>\d+)/$', views.CursosAlumnosUpdate.as_view(), name='editar_cursos_alumnos'),
	url(r'^cursos_alumnos/eliminar/(?P<pk>\d+)/$', views.CursosAlumnosDelete.as_view(), name='eliminar_cursos_alumnos'),
	#Reportes
	url(r'^notas_alumnos$', views.notas_alumnos, name='notas_alumnos'),
	url(r'^promedios_rojos$', views.notas_alumnos_rojo, name='promedios_rojos'),
	#Módulo Configuración Promedio Rojo
	url(r'^promedio_rojo$', views.PromedioRojoList.as_view(), name='promedio_rojo'),
	url(r'^promedio_rojo/crear$', views.PromedioRojoCreate.as_view(), name='crear_promedio_rojo'),
	url(r'^promedio_rojo/editar/(?P<pk>\d+)/$', views.PromedioRojoUpdate.as_view(), name='editar_promedio_rojo'),
]
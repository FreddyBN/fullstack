from django.contrib import admin
from .models import Alumnos
from .models import Profesores
from .models import Cursos
from .models import Pruebas
from .models import Cursos_Alumnos
from .models import Pruebas_Alumnos
# Register your models here.


admin.site.register(Alumnos)
admin.site.register(Profesores)
admin.site.register(Cursos)
admin.site.register(Pruebas)
admin.site.register(Cursos_Alumnos)
admin.site.register(Pruebas_Alumnos)
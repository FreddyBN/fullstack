from django.shortcuts import render, redirect
from django.db import connection
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from .models import Alumnos, Profesores, Cursos, Pruebas, Pruebas_Alumnos, Cursos_Alumnos, PromedioRojo

from .forms import AlumnosForm, ProfesoresForm, CursosForm, PruebasForm, NotasForm, CursosAlumnosForm, PromedioRojoForm

#Home Index
def home(request):
	return render(request, 'colegio/index.html')

#Generic Views para Alumnos
class AlumnosList(ListView):
	model = Alumnos
	template_name = 'colegio/alumnos/alumnos.html'

class AlumnosCreate(CreateView):
	model = Alumnos
	form_class = AlumnosForm
	template_name = 'colegio/alumnos/crear_alumnos.html'
	success_url = reverse_lazy('colegio:alumnos')

class AlumnosUpdate(UpdateView):
	model = Alumnos
	form_class = AlumnosForm
	template_name = 'colegio/alumnos/crear_alumnos.html'
	success_url = reverse_lazy('colegio:alumnos')

class AlumnosDelete(DeleteView):
	model = Alumnos
	template_name = 'colegio/alumnos/borrar_alumnos.html'
	success_url = reverse_lazy('colegio:alumnos')

#Generic Views para Profesores
class ProfesoresList(ListView):
	model = Profesores
	template_name = 'colegio/profesores/profesores.html'

class ProfesoresCreate(CreateView):
	model = Profesores
	form_class = ProfesoresForm
	template_name = 'colegio/profesores/crear_profesores.html'
	success_url = reverse_lazy('colegio:profesores')

class ProfesoresUpdate(UpdateView):
	model = Profesores
	form_class = ProfesoresForm
	template_name = 'colegio/profesores/crear_profesores.html'
	success_url = reverse_lazy('colegio:profesores')

class ProfesoresDelete(DeleteView):
	model = Profesores
	template_name = 'colegio/profesores/borrar_profesores.html'
	success_url = reverse_lazy('colegio:profesores')

#Generic Views para Cursos
class CursosList(ListView):
	model = Cursos
	template_name = 'colegio/cursos/cursos.html'

class CursosCreate(CreateView):
	model = Cursos
	form_class = CursosForm
	template_name = 'colegio/cursos/crear_cursos.html'
	success_url = reverse_lazy('colegio:cursos')

class CursosUpdate(UpdateView):
	model = Cursos
	form_class = CursosForm
	template_name = 'colegio/cursos/crear_cursos.html'
	success_url = reverse_lazy('colegio:cursos')

class CursosDelete(DeleteView):
	model = Cursos
	template_name = 'colegio/cursos/borrar_cursos.html'
	success_url = reverse_lazy('colegio:cursos')

#Generic Views para Pruebas
class PruebasList(ListView):
	model = Pruebas
	template_name = 'colegio/pruebas/pruebas.html'

class PruebasCreate(CreateView):
	model = Pruebas
	form_class = PruebasForm
	template_name = 'colegio/pruebas/crear_pruebas.html'
	success_url = reverse_lazy('colegio:pruebas')

class PruebasUpdate(UpdateView):
	model = Pruebas
	form_class = PruebasForm
	template_name = 'colegio/pruebas/crear_pruebas.html'
	success_url = reverse_lazy('colegio:pruebas')

class PruebasDelete(DeleteView):
	model = Pruebas
	template_name = 'colegio/pruebas/borrar_pruebas.html'
	success_url = reverse_lazy('colegio:pruebas')

#Generic Views para Notas
class NotasList(ListView):
	model = Pruebas_Alumnos
	template_name = 'colegio/notas/notas.html'

class NotasCreate(CreateView):
	model = Pruebas_Alumnos
	form_class = NotasForm
	template_name = 'colegio/notas/crear_notas.html'
	success_url = reverse_lazy('colegio:notas')

class NotasUpdate(UpdateView):
	model = Pruebas_Alumnos
	form_class = NotasForm
	template_name = 'colegio/notas/crear_notas.html'
	success_url = reverse_lazy('colegio:notas')

class NotasDelete(DeleteView):
	model = Pruebas_Alumnos
	template_name = 'colegio/notas/borrar_notas.html'
	success_url = reverse_lazy('colegio:notas')

#Generic Views para CursosAlumnos
class CursosAlumnosList(ListView):
	model = Cursos_Alumnos
	template_name = 'colegio/cursosAlumnos/cursos_alumnos.html'

class CursosAlumnosCreate(CreateView):
	model = Cursos_Alumnos
	form_class = CursosAlumnosForm
	template_name = 'colegio/cursosAlumnos/crear_cursos_alumnos.html'
	success_url = reverse_lazy('colegio:cursos_alumnos')

class CursosAlumnosUpdate(UpdateView):
	model = Cursos_Alumnos
	form_class = CursosAlumnosForm
	template_name = 'colegio/cursosAlumnos/crear_cursos_alumnos.html'
	success_url = reverse_lazy('colegio:cursos_alumnos')

class CursosAlumnosDelete(DeleteView):
	model = Cursos_Alumnos
	template_name = 'colegio/cursosAlumnos/borrar_cursos_alumnos.html'
	success_url = reverse_lazy('colegio:cursos_alumnos')

#Generic Views para Reportes
def notas_alumnos(request):
	cursor = connection.cursor()
	cursor.execute("SELECT a.NOMBRE AS ALUMNO, c.NOMBRE AS CURSO, AVG(p.NOTA) AS PROMEDIO, COUNT(p.NOTA) AS PRUEBAS FROM colegio_pruebas_alumnos AS p INNER JOIN colegio_cursos_alumnos AS ca ON (ca.ID = p.ID_CURSO_ALUMNO_id) INNER JOIN colegio_cursos AS c ON (c.ID_CURSO = ca.ID_CURSO_id) INNER JOIN colegio_alumnos AS a ON (a.ID_ALUMNO = ca.ID_ALUMNO_id) GROUP BY a.ID_ALUMNO, c.ID_CURSO;")
	promedios = cursor.fetchall()
	contexto = {'promedios':promedios}
	return render(request, 'colegio/reportes/notas_alumnos.html', contexto)

def notas_alumnos_rojo(request):
	cursor = connection.cursor()
	cursor.execute("SELECT a.NOMBRE AS ALUMNO, c.NOMBRE AS CURSO, AVG(p.NOTA) AS PROMEDIO, COUNT(p.NOTA) AS PRUEBAS FROM colegio_pruebas_alumnos AS p INNER JOIN colegio_cursos_alumnos AS ca ON (ca.ID = p.ID_CURSO_ALUMNO_id) INNER JOIN colegio_cursos AS c ON (c.ID_CURSO = ca.ID_CURSO_id) INNER JOIN colegio_alumnos AS a ON (a.ID_ALUMNO = ca.ID_ALUMNO_id) GROUP BY a.ID_ALUMNO, c.ID_CURSO HAVING AVG(p.NOTA) <= (SELECT PROMEDIO FROM colegio_promediorojo WHERE id = 1);")
	promedios_rojos = cursor.fetchall()
	contexto = {'promedios_rojos':promedios_rojos}
	return render(request, 'colegio/reportes/promedios_rojos_alumnos.html', contexto)

#Generic Views para Promedio Rojo
class PromedioRojoList(ListView):
	model = PromedioRojo
	template_name = 'colegio/promedioRojo/promedio_rojo.html'

class PromedioRojoCreate(CreateView):
	model = PromedioRojo
	form_class = PromedioRojoForm
	template_name = 'colegio/promedioRojo/crear_promedio_rojo.html'
	success_url = reverse_lazy('colegio:promedio_rojo')

class PromedioRojoUpdate(UpdateView):
	model = PromedioRojo
	form_class = PromedioRojoForm
	template_name = 'colegio/promedioRojo/crear_promedio_rojo.html'
	success_url = reverse_lazy('colegio:promedio_rojo')
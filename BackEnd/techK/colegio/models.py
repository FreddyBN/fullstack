from django.db import models

# Create your models here.
class Alumnos(models.Model):
	ID_ALUMNO = models.AutoField(primary_key=True)
	NOMBRE = models.CharField(max_length=200)

	def __str__(self):
		return '{}'.format(self.NOMBRE)

class Profesores(models.Model):
	ID_PROFESOR = models.AutoField(primary_key=True)
	NOMBRE = models.CharField(max_length=200)

	def __str__(self):
		return '{}'.format(self.NOMBRE)

class Cursos(models.Model):
	ID_CURSO = models.AutoField(primary_key=True)
	NOMBRE = models.CharField(max_length=200)
	ID_PROFESOR = models.ForeignKey(Profesores, on_delete=models.CASCADE)

	def __str__(self):
		return '{}'.format(self.NOMBRE)

class Pruebas(models.Model):
	ID_PRUEBA = models.AutoField(primary_key=True)
	NOMBRE = models.CharField(max_length=200)
	ID_CURSO = models.ForeignKey(Cursos, on_delete=models.CASCADE)	

	def __str__(self):
		return '{}'.format(self.NOMBRE)

class Cursos_Alumnos(models.Model):
	ID_CURSO = models.ForeignKey(Cursos, on_delete=models.CASCADE)	
	ID_ALUMNO = models.ForeignKey(Alumnos, on_delete=models.CASCADE)	

	class Meta:
		unique_together = [("ID_CURSO", "ID_ALUMNO"),]

	def __str__(self):
		return '{}'.format(self.ID_ALUMNO.NOMBRE)

class Pruebas_Alumnos(models.Model):
	ID_PRUEBA = models.ForeignKey(Pruebas, on_delete=models.CASCADE)
	ID_CURSO_ALUMNO = models.ForeignKey(Cursos_Alumnos, on_delete=models.CASCADE)
	NOTA = models.IntegerField()

	class Meta:
		unique_together = [("ID_PRUEBA", "ID_CURSO_ALUMNO"),]

class PromedioRojo(models.Model):
	PROMEDIO = models.IntegerField()
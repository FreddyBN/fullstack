# Generated by Django 2.0.3 on 2018-03-12 19:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('colegio', '0004_auto_20180312_1453'),
    ]

    operations = [
        migrations.CreateModel(
            name='PromedioRojo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('PROMEDIO', models.IntegerField()),
            ],
        ),
    ]
